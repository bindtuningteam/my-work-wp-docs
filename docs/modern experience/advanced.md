![Add section](../images/modern/12.advancedoptions.png)

___
### Documents
Toggles on/off the Documents Tab. If activated, users can see their recent or shared files from OneDrive.

___
### Messages
Toggles on/off the Messages Tab. If activated, users can see their emails from Outlook.

___
### Events
Toggles on/off the Events Tab. If activated, users can see their calendar from Outlook.

### Title Link 

Note this option is only valid if you have a Title on the Web Part.<br>
On the **Title** of the Web Part, you can add an URL to redirect you to any page. The Web Part will always open the new URL in a new tab of your browser and that option is only visible if you're not in **Edit** mode.

___
### Title

If you click on the Title of the Web Part on the page, you can change the Title to the Web Part that will be shown. When you insert a text there it'll show on the page the **Title**, otherwise, nothing will be visible in the page.

![04.title-text.png](../images/modern/04.title-text.png)

