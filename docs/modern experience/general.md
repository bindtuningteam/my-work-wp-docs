![options](../images/modern/11.general.png)

On the Web Part Properties panel, you've multiple options which you can edit for diferent configuration of the Web Part.

- [Layout Options](./layout)
- [Web Part Appearance](./appearance)
- [Advanced Options](./advanced)
- [Web Part Messages](./message)