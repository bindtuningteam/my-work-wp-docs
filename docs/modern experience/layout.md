## Layout Options

![layout](../images/modern/09.layout.png)

___
### Header Background

1. **No Background** - Applies the default colors for the header
2. **Image** - Shows an image on the header. Just insert the image URL.

    ![header-image](../images/modern/01.image-header.gif)

3. **Solid Color** - Customize header by choosing the background and foreground colors

    ![header-color](../images/modern/08.header-color.gif)


___
### Max Documents
Defines the maximum number of files that appear on the document's tab

___
### Tab's Height

Sets a value, in pixels, for the tab's height. This only affects the opened tab.

